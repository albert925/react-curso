import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Search from '../components/search'
import * as actions from '../../actions/index';

class SearchContainer extends Component{
	state = {
		value: 'crick'
	}

	handleSubmitCa = event => {
		event.preventDefault();
		console.log(this.input.value,'submit');
		//fetch(`http://mipai.com/buscar/${this.input.value}`).then(data => console.log(data))
		this.props.actions.searchAsyncEntities(this.input.value)
	}

	setInputRef = element => {
		this.input = element;
	}

	handleInputChange = event => {
		this.setState({
			//value: event.target.value //this.input.value
			value: event.target.value.replace(/ /g, '-')
		})
	}

	render(){
		return(
			<Search 
				setRef={this.setInputRef} 
				handleSubmit={this.handleSubmitCa} 
				handleChange={this.handleInputChange}
				value ={this.state.value}
			/>
		)
	}
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(actions, dispatch)
	}
}

export default connect(null, mapDispatchToProps)(SearchContainer);