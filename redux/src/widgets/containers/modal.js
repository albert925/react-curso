import React, { Component } from 'react';
import { createPortal } from 'react-dom';

class ModalContainer extends Component {
	render(){
		const modal_container = document.getElementById('modal-container')
		return createPortal(this.props.children, modal_container)
	}
}

export default ModalContainer;