import React, {Component} from 'react';

import RegularError from '../components/regular-error';

class HandleError extends Component{
	state = {
		handleError: false
	}
	/*Mostrar si hay un error en la aplicacion*/
	componentDidCatch(error, info){
		//envia este error a un servicio como Sentry
		this.setState({
			handleError: true,
		});
	}
	render(){
		if (this.state.handleError) {
			return(
				<RegularError />
			)
		}

		return this.props.children;
	}
}

export default HandleError;