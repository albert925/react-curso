import { fromJS } from 'immutable';
import date from '../schemas/index.js'

const initialState = fromJS({
	entities: date.entities,
	categories: date.result.categories,
	search: '',
})

function data(state = initialState, action) {
	switch(action.type){
		case 'SEARCH_ENTITIES': {
			//action.payload.query
			//et results = []
			/*const list = state.data.categories[1].playlist;
			const results = list.filter((item) =>{
				return item.title.includes(action.payload.query)//.includes() busca dentro del filter si es igual al strin del query
			});*/
			/*if (action.payload.query != '' || action.payload.query != undefined) {
				state.data.categories.map(item => {
					return item.playlist.filter(item => {
						return item.title.includes(action.payload.query) && results.push(item)
					});
				});
			}
			return {
				...state,
				search: [results]
			}*/
			return state.set('search', action.payload.query);
		}
		default:
			return state;
	}
}

export default data;