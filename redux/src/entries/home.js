import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import Home from '../pages/containers/home';
import reducer from '../reducers/index';
import { Map as map } from 'immutable';

//import date from '../api.json';
//import date from '../schemas/index.js';

//https://platzi.com/blog/immutablejs-colecciones/

//const initialState = date;
/*const initialState = {
	data: {
		//...date,
		entities: date.entities,
		categories: date.result.categories,
		search: [],
	},
	modal: {
		visibility: false,
		mediaId: null
	}
}*/
//ES5
/*function logger({getState, dispatch}) {
	//return (metodo para despachar el siguiente midlewar) => {
	return (next) => {
		return (action) => {
			console.log('este es mi viejo estado', getState().toJS());
			console.log('vamos a enviar esta acción', action);
			const value = next(action)
			console.log('este es mi nuevo estado', getState().toJS());
			return value
		}
	}
}*/
//ES6
const logger_ = ({getState, dispatch}) => next => action => {
	console.log('este es mi viejo estado', getState().toJS());
	console.log('vamos a enviar esta acción', action);
	const value = next(action)
	console.log('este es mi nuevo estado', getState().toJS());
	return value
}

/*
	https://github.com/xgrommx/awesome-redux
	https://github.com/evgenyrodionov/redux-logger
	https://github.com/zalmoxisus/redux-devtools-extension
	https://github.com/reduxjs/redux-thunk
*/

const store = createStore(
	reducer,
	map(),
	composeWithDevTools(
		applyMiddleware(
			logger,
			thunk
		)
	)
	//window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

console.log(store.getState());

const homeContainer = document.getElementById('home-container');

//ReactDOM.render(Que voy a renderizar, donde lo haré)
render(
	<Provider store={store}>
		<Home />
	</Provider>
	,
	homeContainer
)//<es un componente>