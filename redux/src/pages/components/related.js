import React from 'react';

import logo from '../../../images/logo.png';

import './related.css';

function Related (props) {
	return (
		<div className="Related">
			<figure>
				<img src={logo} />
			</figure>
		</div>
	)
}

export default Related;