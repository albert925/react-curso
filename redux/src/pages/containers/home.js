import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { List as list } from 'immutable';

import HomeLayout from '../components/home-layout';
import Categories from '../../categories/components/categories';
import Related from '../components/related';
import ModalContainer from '../../widgets/containers/modal';
import Modal from '../../widgets/components/modal';
import VideoPlayer from '../../player/containers/video-player';

import HandleError from '../../error/containers/handle-errors.js';

import * as actions from '../../actions/index';

class Home extends Component {
	/*state = {
		modalVisible: false
	}*/
	handleOpneModal = id => {
		/*this.setState({
			modalVisible: true,
			media
		})*/
		this.props.actions.openModal(id)
	}
	handleCloseModal = event => {
		/*this.setState({
			modalVisible: false,
		})*/
		this.props.actions.closeModal()
	}
	/*si el modal es viksible && ponga el modal */
	//https://github.com/paularmstrong/normalizr
	render(){
		const {categories} = this.props;
		console.log(this.props.modal.get('mediaId'))
		return (
			<HandleError>
				<HomeLayout>
					<Related />
					<Categories categories={categories} handleOpenModal={this.handleOpneModal} isLoading={this.props.isLoading}
					search={this.props.search} />
					{
						this.props.modal.get('visibility') &&
						<ModalContainer>
							<Modal handleClick={this.handleCloseModal}>
								<VideoPlayer autoplay mediaId={this.props.modal.get('mediaId')} />
							</Modal>
						</ModalContainer>
					}
				</HomeLayout>
			</HandleError>
		)
	}
}

function mapStateToProps (state, props) {
	/*const categories = state.data.categories.map((categoryId) => {
		return state.data.entities.categories[categoryId]
	})*/

	/*const categories = state.get('data').get('categories').map((categoryId) => {
		return state.get('data').get('entities').get('categories').get(categoryId)
	})*/

	const categories = state.getIn(['data', 'categories']).map((categoryId) => {
		return state.getIn(['data', 'entities', 'categories', categoryId])
	})
	let searchResults = list()
	const search = state.getIn(['data', 'search']);
	if (search) {
		const mediaList = state.get('data').get('entities').get('media');
		searchResults = mediaList.filter((item) => {
			return item.get('author').toLowerCase().includes(search.toLowerCase())
		}).toList();
	}

	return {
		categories: categories,
		//search: state.get('data').get('search')
		search: searchResults,
		modal: state.get('modal'),
		isLoading: state.get('isLoading').get('active')
	}
}

function mapDispatchToProps(dispatch) {
	return {
		//actions: bindActionCreators(acciones, dispatch)
		actions: bindActionCreators(actions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);