import React from 'react';
//import Media from './media';
import MediaContainer from '../containers/media';
import Iconsvideo from '../../icons/components/iconsvideo'

import './playlist.css';

//function Playlist(props) {
//	return(
//		<div onClick={props.handleClick}>
//			{props}
//		</div>
//	)
//}

function Playlist (props) {
	const { id, description, title, playlist } = props;
	return(
		<article>
			<div className="Playlist">
				<div className="flex icons-video">
					<Iconsvideo.Play 
						size={20} 
						color='green'
					/>
					<Iconsvideo.Pause 
						size={20} 
						color='red'
					/>
					<Iconsvideo.Volumen 
						size={20} 
						color='blue'
					/>
					<Iconsvideo.FullScreen 
						size={20} 
						color='black'
					/>
				</div>
				{
					//Foreach = map
					playlist.map(mediaId => {
						//return <Media title={item.title} key={item.id} />//ES6
						return <MediaContainer id={mediaId} key={mediaId} openModal={props.handleOpenModal} /> //ES7
					})
				}
			</div>
		</article>
	)
}

export default Playlist;