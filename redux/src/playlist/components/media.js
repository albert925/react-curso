import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './media.css';

class Media extends Component {
	//ES6|||||||
	/*constructor(props){
		super(props)
		this.state = {
			author: props.author
		}
	}*/
	//ES7|||||||
	state = {
		author: 'crick ss'
	}
	//ES6-------
	/*constructor(props){
		super(props)
		this.handleClick = this.handleClick.bind(this)
	}
	handleClick(event){
		let { title, author, image } = this.props;
		console.log(title)
	}*/
	//ES7-------
	/*handleClick = event => {
		let { title, author, image } = this.props;
		//console.log(title)
		this.setState({
			author: 'crick crick',
		});
	}*/
	handleClick = (event) => {
		console.log(this.props)
		this.props.openModal(this.props.id)
	}
	render(){
		let { title, author, cover } = this.props;
		return(
			<div className="Media" onClick={this.handleClick}>
				<div className="Media-cover">
					<img className="Media-image" 
						src={cover} 
						alt="" 
						width={240} 
						height={160}
					/>
					<h3 className="Media-title">{title}</h3>
					<p className="Media-author">{author}</p>
				</div>
			</div>
		)
	}
}

//https://reactjs.org/docs/typechecking-with-proptypes.html
//--------eventos-----------
//https://reactjs.org/docs/handling-events.html

Media.propTypes = {
	cover: PropTypes.string,
	title: PropTypes.string.isRequired,
	author: PropTypes.string,
	type: PropTypes.oneOf(['video', 'audio']),
}

/*
PropTypes.number
PropTypes.object
PropTypes.func
PropTypes.array
*/

export default Media;
