import React from 'react';
import Iconsvideo from '../../icons/components/iconsvideo'
import './full-screen.css';

function FullScreen(props) {
	return (
		<div 
			className="FullScreen" 
			onClick={props.handleFullScreenClick}
		>
			<Iconsvideo.FullScreen 
				size={25} 
				color="white"
			/>
		</div>
	)
}

export default FullScreen;