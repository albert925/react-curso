import React from 'react';
import Iconsvideo from '../../icons/components/iconsvideo'
import './play-pause.css'

function PlayPause(props) {
	return(
		<div className="PlayPause">
			{
				props.pause ?
					<button 
						onClick={props.handleClick}
					>
						<Iconsvideo.Play size={25} color="white" />
					</button>
				:
					<button 
						onClick={props.handleClick} 
					>
						<Iconsvideo.Pause size={25} color="white" />
					</button>
			}
		</div>
	)
}

export default PlayPause;