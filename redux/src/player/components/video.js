import React, {Component} from 'react';

import './video.css';

//https://reactjs.org/docs/events.html

class Video extends Component {
	togglePlay(){
		if (this.props.pause) {
			this.video.play()
		}
		else{
			this.video.pause()
		}
	}
	componentWillReceiveProps(nextProps){
		if (nextProps.pause != this.props.pause) {
			this.togglePlay();
		}
	}
	setRef = element => {
		this.video = element;
	}
	render(){
		const {
			autoplay,
			src,
			handleLoadedMetadata,
			handleTimeUpdate,
			handleeeking,
			handleSeeked
		} = this.props;
		//onSeeking = hacer el movimiento
		//onSeeked = ya se movio
		return(
			<div className="Video">
				<video  
					autoPlay={autoplay} 
					src={src} 
					ref={this.setRef} 
					onLoadedMetadata={handleLoadedMetadata} 
					onTimeUpdate={handleTimeUpdate} 
					onSeeking={handleeeking}
					onSeeked={handleSeeked}
				/>
			</div>
		)
	}
}

export default Video;