import React from 'react';
import Iconsvideo from '../../icons/components/iconsvideo'
import './volume.css';

function Volume(props) {
	return(
		<button className="Volume">
			<div onClick={props.handleClick}>
				<Iconsvideo.Volumen 
					color="white" 
					size={25} 
				/>
			</div>
			<div className="Volume-range">
				<input 
					type="range" 
					min={0} 
					max={1} 
					step={.05} 
					onChange={props.handleVolumeChange} 
					value={props.value}
				/>
			</div>
		</button>
	)
}

export default Volume;