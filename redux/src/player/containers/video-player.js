import React, {Component} from 'react';
import { connect } from 'react-redux';

import VideoPlayerLayout from '../components/video-player-layout';
import Video from '../components/video';
import Title from '../components/title';
import PlayPause from '../components/play-pause';
import Timer from '../components/timer'
import Controls from '../components/video-player-controls'; //import cualquierNombre from ruta
import ProgressBar from '../components/progress-bar';
import Spinner from '../components/spinner';
import Volume from '../components/Volume';
import FullScreen from '../components/full-screen'
import Utils from '../../utils/utils';

class VideoPlayer extends Component{
	state = {
		pause: true,
		duration: 0,
		currentTime: 0,
		timeEnd: '',
		loading: false,
		lastVolumeState: null,
		volume: 1
	}

	toggleClick = (event) => {
		this.setState({
			pause: !this.state.pause //true a false
		})
	}
	componentDidMount(){
		this.setState({
			pause: (!this.props.autoplay)
		})
	}
	handleLoadedMetadata = event => {
		this.video = event.target;
		this.setState({
			duration: this.video.duration
		})
	}
	handleTimeUpdate = event => {
		//console.log(this.video.currentTime)
		this.setState({
			currentTime: this.video.currentTime
		})
	}
	handleProgressChange = event => {
		this.video.currentTime = event.target.value
	}
	handleeeking = event => {
		this.setState({
			loading: true
		})
	}
	handleSeeked = event => {
		this.setState({
			loading: false
		})
	}
	handleVolumeChange = event => {
		this.video.volume = event.target.value;
	}
	handleVolumeClick = event => {
		console.log(this.video.volume)
		if (this.video.volume !== 0) {
			this.mute() 
		}
		else{
			this.unmute()
		}
		console.log(this.video.volume)
	}
	handleFullScreenClick = event => {
		if (!document.webkitIsFullScreen) {
			this.player.webkitRequestFullScreen()
		}
		else if (!document.mozFullScreen) {
			this.player.mozRequestFullscreen();
		}
		else if (!document.msFullScreen) {
			this.player.msRequestFullScreen();
		}
		else{
			if (document.webkitIsFullScreen) {
				document.webkitExitFullScreen();
			}
			else if (document.mozFullScreen) {
				document.mozCancelFullScreen()
			}
		}
	}
	durationTime = (duration, time) => {
		var duration = Utils.formattedTime(duration);
		var curren = Utils.formattedTime(time);

		return `${duration} / ${curren}`;
	}
	mute = () => {
		const lastState = this.video.volume;
		this.setState({
			lastVolumeState: lastState,
			volume: 0
		})
		this.video.volume = 0
	}
	unmute = () => {
		this.setState({
			volume: this.state.lastVolumeState
		})
		this.video.volume = this.state.lastVolumeState
	}
	setRef = element => {
		this.player = element
	}
	render(){
		return(
			<VideoPlayerLayout 
				setRef={this.setRef}
			>
				<Title title={this.props.media.get('title')} />
				<Controls>
					<PlayPause 
						pause={this.state.pause} 
						handleClick={this.toggleClick} 
					/>
					<Timer 
						duration={this.state.duration} 
						currentTime={this.state.currentTime} 
						durationTime={this.durationTime(this.state.duration, this.state.currentTime)}
					/>
					<ProgressBar 
						duration={this.state.duration} 
						value={this.state.currentTime} 
						handleProgressChange={this.handleProgressChange}
					/>
					<Volume 
						handleVolumeChange={this.handleVolumeChange} 
						handleClick={this.handleVolumeClick} 
						value={this.state.volume}
					/>
					<FullScreen 
						handleFullScreenClick={this.handleFullScreenClick} 
					/>
				</Controls>
				<Spinner 
					active={this.state.loading}
				/>
				<Video 
					autoplay={this.props.autoplay} 
					pause={this.state.pause} 
					handleLoadedMetadata={this.handleLoadedMetadata} 
					handleTimeUpdate={this.handleTimeUpdate} 
					src={this.props.media.get('src')} 
					handleeeking={this.handleeeking}
					handleSeeked={this.handleSeeked}
				/>
			</VideoPlayerLayout>
		)
	}
}

function mapStateToProps (state, props) {
	return {
		media: state.get('data').get('entities').get('media').get(props.mediaId)
	}
}

export default connect(mapStateToProps)(VideoPlayer);