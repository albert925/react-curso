export function openModal(mediaId) {
	return {
		type: 'OPEN_MODAL',
		payload: {
			mediaId: mediaId
		}
	}
}

export function closeModal() {
	return {
		type: 'CLOSE_MODAL',
	}
}

export function searchEntities(query) {
	return {
		type: 'SEARCH_ENTITIES',
		payload: {
			query: query
		}
	}
}

export function searchAsyncEntities(query) {
	return (dispatch) => {
		//fetch().then((res) => {dispatch(searchEntities(res))})
		//XHR
		//trae
		dispatch(isLoading(true))
		setTimeout(() => {
			dispatch(isLoading(false))
			dispatch(searchEntities(query))
		}, 5000)
	}
}

export function isLoading(value) {
	return {
		type: 'IS_LOADING',
		payload: {
			value
		}
	}
}