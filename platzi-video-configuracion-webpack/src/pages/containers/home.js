import React, {Component} from 'react';

import HomeLayout from '../components/home-layout';
import Categories from '../../categories/components/categories';
import Related from '../components/related';
import ModalContainer from '../../widgets/containers/modal';
import Modal from '../../widgets/components/modal';
import VideoPlayer from '../../player/containers/video-player';

import HandleError from '../../error/containers/handle-errors.js';

class Home extends Component {
	state = {
		modalVisible: false
	}
	handleOpneModal = media => {
		this.setState({
			modalVisible: true,
			media
		})
	}
	handleCloseModal = event => {
		this.setState({
			modalVisible: false,
		})
	}
	/*si el modal es viksible && ponga el modal */
	render(){
		const {data} = this.props;

		return (
			<HandleError>
				<HomeLayout>
					<Related />
					<Categories categories={data.categories} handleOpenModal={this.handleOpneModal} />
					{
						this.state.modalVisible &&
						<ModalContainer>
							<Modal handleClick={this.handleCloseModal}>
								<VideoPlayer autoplay src={this.state.media.src} title={this.state.media.title} />
							</Modal>
						</ModalContainer>
					}
				</HomeLayout>
			</HandleError>
		)
	}
}

export default Home;