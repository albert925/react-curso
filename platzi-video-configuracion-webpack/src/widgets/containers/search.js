import React, { Component } from 'react';

import Search from '../components/search'

class SearchContainer extends Component{
	state = {
		value: 'crick'
	}

	handleSubmitCa = event => {
		event.preventDefault();
		console.log(this.input.value,'submit');
	}

	setInputRef = element => {
		this.input = element;
	}

	handleInputChange = event => {
		this.setState({
			//value: event.target.value //this.input.value
			value: event.target.value.replace(/ /g, '-')
		})
	}

	render(){
		return(
			<Search 
				setRef={this.setInputRef} 
				handleSubmit={this.handleSubmitCa} 
				handleChange={this.handleInputChange}
				value ={this.state.value}
			/>
		)
	}
}

export default SearchContainer;