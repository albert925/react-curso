import React from 'react';

import './search.css';

//function Search(props) {
//	return(
//		<form action=""></form>
//	)
//}

//defaultValue="date"

const Search = (props) => (
	<form 
		action='.' 
		className="Search" 
		onSubmit={props.handleSubmit} 
	>
		<input 
			ref={props.setRef} 
			className="Search-input" 
			type="text" 
			name="search" 
			id="search" 
			placeholder="Busca tu video favorito" 
			onChange={props.handleChange} 
			value={props.value}
		/>
	</form>
)

export default Search;