import React, { Component } from 'react';
import Playlist from './playlist';

class Contenidos extends Component{
	render(){
		const { data } = this.props
		const categorias = data.categories

		return (
			<section className="categorias">
				{
					categorias.map((item) => {
						return <Playlist {...item} key={item.id} />
					})
				}
			</section>
		)
	}
}

export default Contenidos;
