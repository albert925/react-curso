const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
	entry: path.resolve(__dirname, 'index.js'),
	output:{
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: './dist/'
	},
	module:{
		rules: [
			// aqui va los loaders
			{
				/*test: que tipo de archivo quiero reconocer,
				user: que loader se va a encargar el archivo*/
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					//['style-loader','css-loader']
					//fallback: 'style-loade',
					use: 'css-loader'
				}),
				/*{ loader: "style-loader" }, // Agrega el css al DOM en un <style>
				{ loader: "css-loader" }, // interpreta los archivos css en js via import*/
			}
		]
	},
	plugins: [
		//aqui va los plugins
		//new ExtractTextPlugin('css/styles.css')
		new ExtractTextPlugin('css/[name].css')
	]
}

module.exports = config;