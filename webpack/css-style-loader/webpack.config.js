const path = require('path');

const config = {
	entry: path.resolve(__dirname, 'index.js'),
	output:{
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: './dist/'
	},
	module:{
		rules: [
			// aqui va los loaders
			{
				/*test: que tipo de archivo quiero reconocer,
				user: que loader se va a encargar el archivo*/
				test: /\.css$/,
				use: ['style-loader','css-loader'],
				/*{ loader: "style-loader" }, // Agrega el css al DOM en un <style>
				{ loader: "css-loader" }, // interpreta los archivos css en js via import*/
			}
		]
	}
}

module.exports = config;